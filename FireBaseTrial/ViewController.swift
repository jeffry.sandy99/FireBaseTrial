//
//  ViewController.swift
//  SwitchingSiri
//
//  Created by Jeffry Sandy Purnomo on 08/02/21.
//

import UIKit

class ViewController: UIViewController {

    private let profileButton: UIButton = {
        let profileButton = UIButton()
        profileButton.setImage(UIImage(systemName: "gearshape.fill"), for: .normal)
        profileButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        profileButton.contentVerticalAlignment = .fill
        profileButton.contentHorizontalAlignment = .fill
        profileButton.imageView?.contentMode = .scaleAspectFit
        profileButton.addTarget(self, action: #selector(buttonClicked(_ :)), for: .touchUpInside)
        profileButton.backgroundColor = .white
        profileButton.layer.cornerRadius = 10
        profileButton.tintColor = .systemBlue
        profileButton.clipsToBounds = true
        profileButton.translatesAutoresizingMaskIntoConstraints = false
        
        return profileButton
    }()
    
    private let myView: UIView = {
        let myView = UIView()
        myView.translatesAutoresizingMaskIntoConstraints = false
        myView.backgroundColor = .link
        
        return myView
    }()
    
    private var dashboardCollectionView: UICollectionView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 10
        layout.itemSize = CGSize(width: (view.frame.size.width / 4) - 20, height: (view.frame.size.width / 4) - 20)
        
        dashboardCollectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        guard let dashboardCollectionView = dashboardCollectionView else {return}
        dashboardCollectionView.translatesAutoresizingMaskIntoConstraints = false
        
        dashboardCollectionView.register(DeviceCollectionViewCell.self, forCellWithReuseIdentifier: "DeviceCollectionViewCell")
        dashboardCollectionView.dataSource = self
        
        view.addSubview(myView)
        view.addSubview(profileButton)
        view.addSubview(dashboardCollectionView)
        dashboardCollectionView.frame = view.bounds
        dashboardCollectionView.backgroundColor = .clear
        
        dashboardCollectionView.delegate = self
        addConstraints()
    }
    
    @objc func buttonClicked(_ sender: UIButton){
        print("U Did it")
    }
    
    func addConstraints(){
        var constraints = [NSLayoutConstraint]()
        //View
        constraints.append(myView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor ))
        constraints.append(myView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor))
        constraints.append(myView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor ))
        constraints.append(myView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor ))
        
        //profileButton
        constraints.append(profileButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20))
        constraints.append(profileButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20))
        constraints.append(profileButton.widthAnchor.constraint(equalToConstant: 55))
        constraints.append(profileButton.heightAnchor.constraint(equalToConstant: 44))
        
        constraints.append(dashboardCollectionView!.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20 ))
        constraints.append(dashboardCollectionView!.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20))
        constraints.append(dashboardCollectionView!.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 200))
        constraints.append(dashboardCollectionView!.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor ))
        //Activate
        NSLayoutConstraint.activate(constraints)
        
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DeviceCollectionViewCell", for: indexPath) as! DeviceCollectionViewCell
        
        cell.configure(label: "\(indexPath.row)")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("This is Row \(indexPath.row)")
    }
    
    
}
