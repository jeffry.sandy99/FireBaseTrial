//
//  DeviceDataModel.swift
//  SwitchingSiri
//
//  Created by Jeffry Sandy Purnomo on 08/02/21.
//

import Foundation

struct DeviceResponse: Decodable{
    var response: [DeviceType]
}

struct DeviceType: Decodable{
    var name: String
}
