//
//  DeviceCollectionViewCell.swift
//  SwitchingSiri
//
//  Created by Jeffry Sandy Purnomo on 08/02/21.
//

import UIKit

class DeviceCollectionViewCell: UICollectionViewCell{
    static let identifier = "DeviceCollectionViewCell"
    
    private let myImageView: UIImageView = {
        let imageView = UIImageView()
        
        imageView.image = UIImage(systemName: "lightbulb")
//        imageView.backgroundColor = .systemGray
        imageView.contentMode = .scaleAspectFit
        
        return imageView
    }()
    
    private let myLabel: UILabel = {
        let label = UILabel()
        
        label.text = "Light Bulb"
//        label.backgroundColor = .systemGreen
        label.textAlignment = .center
        
        return label
    }()
    override init(frame: CGRect){
        super.init(frame: frame)
        
        contentView.backgroundColor = .systemRed
        contentView.addSubview(myLabel)
        contentView.addSubview(myImageView)
        contentView.layer.cornerRadius = 20
        contentView.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        myLabel.frame = CGRect(x: 5, y: contentView.frame.size.height - 30, width: contentView.frame.size.width - 10, height: 30)
        myImageView.frame = CGRect(x: 5, y: 0, width: contentView.frame.size.width - 10, height: contentView.frame.size.height - 30)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func configure(label: String){
        myLabel.text = label
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        myLabel.text = nil
    }
}
