//
//  DashboardViewController.swift
//  SwitchingSiri
//
//  Created by Jeffry Sandy Purnomo on 08/02/21.
//

import UIKit

class DashboardViewController: UIViewController{
    
    var button = UIButton.init(type: .roundedRect)
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
     
        button.setTitle("Register New Device", for: .normal)
        button.addTarget(self, action: #selector(buttonClicked(_ :)), for: .touchUpInside)
        button.backgroundColor = .systemBlue
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 12
        button.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(button)
        
        view.backgroundColor = .systemOrange
        addConstraints()
    }
    
    @objc func buttonClicked(_ sender: UIButton){
        print("U Did it")
    }
    
    func addConstraints(){
        var constraints = [NSLayoutConstraint]()
        
        constraints.append(button.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -100))
        constraints.append(button.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor))
        constraints.append(button.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20))
        constraints.append(button.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20))
        constraints.append(button.heightAnchor.constraint(equalToConstant: 44))
        
        NSLayoutConstraint.activate(constraints)
    }
}
